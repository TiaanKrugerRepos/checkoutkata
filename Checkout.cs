﻿using System;
using System.Collections.Generic;   

namespace CheckoutKata
{
    public class Checkout
    {

        private Dictionary<string, decimal> _prices;
        private Dictionary<string, decimal> _itemCounters;
        private Dictionary<string, ItemDiscount> _discount;

        /// <summary>
        /// Initializes a new instance of the <see cref="Checkout"/> class.
        /// </summary>
        public Checkout()
        {
            _itemCounters = new Dictionary<string, decimal> { { "A99", 0m }, { "B15", 0m }, { "C40", 0m } };
            _prices = new Dictionary<string, decimal> { { "A99", 0.50m }, { "B15", 0.30m }, { "C40", 0.60m } };
            _discount = new Dictionary<string, ItemDiscount>
            {
                {"A99", new ItemDiscount { Discount = 0.20m, NumberOfItems = 3}},
                {"B15", new ItemDiscount { Discount = 0.15m, NumberOfItems = 2}},
            };
        }

        /// <summary>
        /// The total
        /// </summary>
        private decimal Total;

        /// <summary>
        /// Gets the total.
        /// </summary>
        /// <returns></returns>
        public decimal GetTotal()
        {
            decimal sumTotal = 0m;

            foreach (var itemCounter in _itemCounters)
            {
                Total = Total + calculateSubTotal(itemCounter.Key, itemCounter.Value);
            }

            return Total;
        }

        public decimal calculateSubTotal(string Code, decimal amount)
        {
            decimal preDiscountSum = _prices[Code] * amount;

            if (_discount.ContainsKey(Code))
            {
                
                decimal discountSum =  Math.Floor(amount / _discount[Code].NumberOfItems) * _discount[Code].Discount;

                return preDiscountSum - discountSum;
            }

            return preDiscountSum;

        }

        /// <summary>
        /// Scans the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        public void Scan(string item)
        {
           _itemCounters[item]++;
           //Total += _prices[item];
           //if (HasOffer(item) && RequiredNumber(item))
           //{
           //    Total -= _discount[item].Discount;
           //}
        }

        /// <summary>
        /// Determines whether the specified item has offer.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>
        ///   <c>true</c> if the specified item has offer; otherwise, <c>false</c>.
        /// </returns>
        private bool HasOffer(string item)
       {
           return _discount.ContainsKey(item);
       }

        /// <summary>
        /// checks if required amount of the item is present.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        private bool RequiredNumber(string item)
       {
           return _itemCounters[item] == _discount[item].NumberOfItems;
       }

    }
}
