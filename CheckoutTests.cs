﻿using System.Collections.Generic;
using NUnit.Framework;

namespace CheckoutKata
{
    [TestFixture]
    public class CheckoutTests
    {
        [Test]
        public void ScanNoItems_TotalIsZero()
        {
            // Arrange
            var checkout = new Checkout();

            // Act + Assert
            Assert.AreEqual(0, checkout.GetTotal());
        }

        /// <summary>
        /// The singles lists
        /// </summary>
        private static readonly object[] SinglesLists =
        {
            new object[] { "A99" , 0.50m},                    //case 1
            new object[] { "B15" , 0.30m},                    //case 2
            new object[] { "C40" , 0.60m},                    //case 3
        };

        /// <summary>
        /// The no discount lists
        /// </summary>
        private static readonly object[] NoDiscountLists =
        {
            new object[] {new List<string> { "A99","A99" }, 1.00m},                    //case 1
            new object[] {new List<string> { "A99", "A99","B15" }, 1.30m},                    //case 2
            new object[] {new List<string> { "A99", "A99", "C40" }, 1.60m},                    //case 3
            new object[] {new List<string> { "A99", "B15" }, 0.80m},             //case 4
            new object[] {new List<string> { "A99", "B15", "C40" }, 1.40m},      //case 5
        };

        /// <summary>
        /// The discount lists
        /// </summary>
        private static readonly object[] DiscountLists =
        {
            new object[] {new List<string> { "A99", "A99", "A99" }, 1.30m},                    //case 1
            new object[] {new List<string> { "B15", "B15" }, 0.45m},                    //case 2
            new object[] {new List<string> { "A99", "A99", "A99", "B15", "B15" }, 1.75m},                    //case 3
            new object[] {new List<string> { "A99", "A99", "A99", "B15" }, 1.60m},             //case 4
            new object[] {new List<string> { "B15", "B15", "C40" }, 1.05m},      //case 5
        };

        /// <summary>
        /// The multi discount lists
        /// </summary>
        private static readonly object[] MultiDiscountLists =
        {
            new object[] {new List<string> { "A99", "A99", "A99", "A99", "A99", "A99" }, 2.60m},                    //case 1
            new object[] {new List<string> { "A99", "A99", "A99", "A99", "A99", "A99", "A99" }, 3.10m},                    //case 1
        };

        [TestCaseSource("SinglesLists")]
        public void ScanOneItem_TotalIsValueItem(string item, decimal total)
        {
            // Arrange
            var checkout = new Checkout();

            // Act
            checkout.Scan(item);

            // Assert
            Assert.AreEqual(total, checkout.GetTotal());
        }
        
        [TestCaseSource("NoDiscountLists")]
        public void ScanMultipleItems_TotalIsSum(List<string> items, decimal total)
        {
            ScanMultipleItems(items: items, total: total);
        }

        [TestCaseSource("DiscountLists")]
        public void ScanMultipleItemsWithOffer_TotalIsSumSubtractDiscount(List<string> items, decimal total)
        {
            ScanMultipleItems(items, total);
        }

        [TestCaseSource("MultiDiscountLists")]
        public void ScanMultipleItemsMultiDiscount_TotalIsSum(List<string> items, decimal total)
        {
            ScanMultipleItems(items: items, total: total);
        }

        /// <summary>
        /// Scans multiple items.
        /// </summary>
        /// <param name="items">The items.</param>
        /// <param name="total">The total.</param>
        public void ScanMultipleItems(List<string> items, decimal total)
        {
            // Arrange
            var checkout = new Checkout();

            // Act
            foreach (string item in items)
            {
                checkout.Scan(item);
            }

            // Assert
            Assert.AreEqual(total, checkout.GetTotal());
        }

    } 
}
