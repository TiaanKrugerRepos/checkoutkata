﻿namespace CheckoutKata
{
    public class ItemDiscount
    {
        /// <summary>
        /// Gets or sets the discount.
        /// </summary>
        /// <value>
        /// The discount.
        /// </value>
        public decimal Discount { get; set; }

        /// <summary>
        /// Gets or sets the number of items.
        /// </summary>
        /// <value>
        /// The number of items.
        /// </value>
        public int NumberOfItems { get; set; }
    }
}